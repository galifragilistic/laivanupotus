﻿using System;
using NUnit.Framework;

namespace PelilautaPahkina.Tests
{
    [TestFixture]
    public class Tests
    {
        private Battlefield _battlefield;

        [SetUp]
        public void Deserialize()
        {
            _battlefield = Newtonsoft.Json.JsonConvert.DeserializeObject<Battlefield>(
                System.IO.File.ReadAllText(TestContext.CurrentContext.TestDirectory + "\\sotatanner.json"));
            Assert.IsNotNull(_battlefield);
        }

        [Test]
        public void TestGetBoardCoordinate()
        {
            var board = new Board();

            var coordinate = board.GetCoordinate("a1");
            Assert.IsTrue(coordinate.Vertical == "a" && coordinate.Horizontal == "1");

            coordinate = board.GetCoordinate(0);
            Assert.IsTrue(coordinate.Vertical == "a" && coordinate.Horizontal == "1");
            coordinate = board.GetCoordinate(9);
            Assert.IsTrue(coordinate.Vertical == "a" && coordinate.Horizontal == "10");
            coordinate = board.GetCoordinate(10);
            Assert.IsTrue(coordinate.Vertical == "b" && coordinate.Horizontal == "1");
            coordinate = board.GetCoordinate(19);
            Assert.IsTrue(coordinate.Vertical == "b" && coordinate.Horizontal == "10");
            coordinate = board.GetCoordinate(20);
            Assert.IsTrue(coordinate.Vertical == "c" && coordinate.Horizontal == "1");

        }   
    }
}
