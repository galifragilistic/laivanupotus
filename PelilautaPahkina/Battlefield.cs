﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace PelilautaPahkina
{
    public class Battlefield
    {
        [JsonProperty(PropertyName = "nappulat")]
        public List<string[]> ShipPieces { get; set; }

        [JsonProperty(PropertyName = "shotsFired")]
        public List<string> ShotsFired { get; set; }
    }
}
