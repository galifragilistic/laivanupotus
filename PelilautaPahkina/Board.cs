﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PelilautaPahkina
{
    
    public class Board  
    {
        private List<BoardCoordinate> BoardCoordinates { get; }

        private static readonly string[] Alphabet = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };

        public Board()
        {
            BoardCoordinates = new List<BoardCoordinate>();
            for (int vertical = 0; vertical < 10; vertical++)
            {
                for (int horizontal = 1; horizontal <= 10; horizontal++)
                {
                    BoardCoordinates.Add(new BoardCoordinate
                    {
                        Vertical = Alphabet[vertical],
                        Horizontal = horizontal.ToString(),
                        Value = BoardValue.Empty
                    });
                }
            }
        }

        public void PlaceShips(IEnumerable<string[]> ships)
        {
            foreach (string[] ship in ships)
            {
                foreach (string coordinate in ship)
                {
                   SetValue(coordinate, BoardValue.Ship);
                }
            }
        }

        public void FireShots(List<string> shotsFired)
        {
            for (int i = 0; i < 100; i++)
            {
                var coor = GetCoordinate(i);

                // anything other than an empty string in the input counts as a shot
                if (string.IsNullOrWhiteSpace(shotsFired[i]) == false)
                {
                    if (coor.Value == BoardValue.Ship)
                    {
                        coor.Value = BoardValue.Hit;
                    }
                    else
                    {
                        coor.Value = BoardValue.Miss;
                    }
                }
            }
        }

        public void DrawBoard()
        {
            Console.WriteLine("Post Game Analysis :");
            for (int vertical = 0; vertical < 10; vertical++)       
            {
                Console.WriteLine("---------------------");

                var ver = Alphabet[vertical];
                var coords = BoardCoordinates
                    .Where(c => c.Vertical == ver)
                    .OrderBy(c => int.Parse(c.Horizontal))
                    .Select(c => c.ToString());

                Console.Write("|");
                Console.Write(coords.Aggregate((c1, c2) => c1 + "|" + c2));
                Console.Write("|\n");
            }
            Console.WriteLine("---------------------");
            Console.WriteLine("* = Boatpiece, o = missed shot, X = DIRECT HIT!");
        }

        public BoardCoordinate GetCoordinate(string coordinate)
        {
            var ver = coordinate[0];
            var hor = coordinate.TrimStart(ver);

            var coor = BoardCoordinates.FirstOrDefault(c => c.Vertical == ver.ToString() && c.Horizontal == hor);

            if (coor == null)
            {
                throw new NullReferenceException($"Coordinate not found: {coordinate}");
            }

            return coor;
        }

        public BoardCoordinate GetCoordinate(int iterator)
        {
            string vertical = Alphabet[iterator / 10];
            int horizontal = (iterator % 10) + 1;
            string coordinate = $"{vertical}{horizontal}";

            return GetCoordinate(coordinate);
        }

        private void SetValue(string coordinate, BoardValue value)
        {
            var coor = GetCoordinate(coordinate);
            if (coor != null) coor.Value = value;
        }
    }
}

public enum BoardValue
{
    Empty,
    Ship,
    Hit,
    Miss
}

public class BoardCoordinate
{
    public string Vertical { get; set; }
    public string Horizontal { get; set; }
    public BoardValue Value { get; set; }

    public override string ToString()
    {
        switch (Value)
        {
            case BoardValue.Empty:
                return " ";
            case BoardValue.Hit:
                return "X";
            case BoardValue.Miss:
                return "o";
            case BoardValue.Ship:
                return "*";
            default:
                return " ";
        }
    }
}
