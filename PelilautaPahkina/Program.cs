﻿using System;

namespace PelilautaPahkina
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var board = new Board();

            var battlefield = Newtonsoft.Json.JsonConvert.DeserializeObject<Battlefield>(
                System.IO.File.ReadAllText("sotatanner.json"));

            board.PlaceShips(battlefield.ShipPieces);

            board.FireShots(battlefield.ShotsFired);

            board.DrawBoard();

            Console.ReadKey();
        }        
    }
}

